<?php

namespace backend\modules\setting\umum\controllers;

use Yii;

use yii\base\DynamicModel;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\httpclient\Client;

class RegistrasiController extends \yii\web\Controller
{
    public function actionIndex(){
		
		$cekRegistrasi = 'SELECT `Value` FROM settingparameters WHERE Name = "KodeRegistrasi"';
        $cekRegistrasi = Yii::$app->db->createCommand($cekRegistrasi)->queryOne();
		// $out = \common\components\HarvestTajukSubjek::harvesttajuksubjek('2010-11-05','http://opac.perpusnas.go.id/inlis_web_service/mtom.asmx/GetAuthorityData?reqlastdate=');
		// echo"<pre>";
		// print_r($out);
		// die;
		// $getIP = getHostByName(getHostName());
        return $this->render('index',['data' => $cekRegistrasi]);
    }

    public function actionProses(){
        if($_POST){
            $cekSettingParam = Yii::$app->db->createCommand('SELECT `Name` FROM settingparameters WHERE `Name` = "KodeRegistrasi"')->queryOne();
            if($cekSettingParam){
                // echo 'b';die;
                // $out = \common\components\Registrasi::registrasi($_POST,'http://localhost/osss/perpusdaerah/api/web/v1/registrasi?');
                // $out = \common\components\Registrasi::registrasi($_POST,'http://suryaciptaagung.co.id/OSSS/Perpus%20Daerah/api/web/v1/registrasi?');

                $out = \common\components\Registrasi::registrasi($_POST,'http://demo.inlislitev31.perpusnas.go.id/api/web/v1/registrasi?');

                if ($out){
                    echo json_encode(Array('status' => 'TRUE'));
                    $data = Yii::$app->db->createCommand('UPDATE settingparameters SET `Value` = "'.$_POST['kodeRegis'].'" WHERE Name = "KodeRegistrasi"')->execute();
                } else {
                    echo json_encode(Array('status' => 'FALSE'));
                }
            }else{
                // echo 'aa';die;
                // $out = \common\components\Registrasi::registrasi($_POST,'http://localhost/osss/perpusdaerah/api/web/v1/registrasi?');
                // $out = \common\components\Registrasi::registrasi($_POST,'http://suryaciptaagung.co.id/OSSS/Perpus%20Daerah/api/web/v1/registrasi?');

                $out = \common\components\Registrasi::registrasi($_POST,'http://demo.inlislitev31.perpusnas.go.id/api/web/v1/registrasi?');

                if ($out){
                    echo json_encode(Array('status' => 'TRUE'));
                    $data = Yii::$app->db->createCommand('INSERT settingparameters SET `Name` = "KodeRegistrasi", `Value` = "'.$_POST['kodeRegis'].'"')->execute();
                } else {
                    echo json_encode(Array('status' => 'FALSE'));
                }
            }
            

            /*if($data){
                echo json_encode(Array('status' => 'TRUE'));
            }else{
                echo json_encode(Array('status' => 'FALSE'));
            }*/
            
        }else{
            echo json_encode(Array('status' => 'FALSE'));
        }
    }

    public function actionGetip(){
        $client = new Client(['baseUrl' => 'http://ip-api.com/']);
        $response = $client->get('json')->send();
        echo'<pre>';print_r($response);
    }

    public function actionDetail(){
        // $api = \common\components\Registrasi::detail($_GET, 'http://suryaciptaagung.co.id/OSSS/Perpus%20Daerah/api/web/v1/registrasi/view');

        $api = \common\components\Registrasi::detail($_GET, 'http://demo.inlislitev31.perpusnas.go.id/api/web/v1/registrasi/view');
    }

    public function actionGetProv(){
        $prov = Yii::$app->db->createCommand('SELECT ID, NamaPropinsi FROM propinsi WHERE ID = "'.$_GET['prov'].'"')->queryOne();
        echo json_encode($prov);
    }

}